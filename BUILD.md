# Building CLParser

1. Via cmake / make
2. Via Docker

### Cmake / Make
In order to build with cmake, run the following:

```
mkdir build
cd build
cmake ..
make -j4
```

The static library will be in the build/lib directory.  A test program will be in the build/bin directory.

### Docker
To build using docker, run:

#### Debug build
```
docker build --target debug -t clparser .
```

#### Release build
```
docker build --target release -t clparser .
```
