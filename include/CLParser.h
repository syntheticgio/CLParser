//
// Created by syntheticgio on 7/9/21.
//

#ifndef CLPARSER_CLPARSER_H
#define CLPARSER_CLPARSER_H

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <cstring>
#include <iostream>
#include <cstring>
#include <algorithm>

class CLParser {
public:
	enum ArgType {
		kNone = 0,
		kInt = 1,
		kString = 2,
		kBool = 3,
		kFloat = 4,
		kArray = 5
	};

	enum FetchValue {
	    kFetchValueNone = 0,
	    kFetchValueSuccess = 1,
	    kFetchValueNoValueEntered = 2,
	    kFetchValueErrorNoArgRegistered = 3,
	    kFetchValueErrorWrongType = 4
	};



	struct Argument {
		Argument() : required(false), help("Default help message."), arg_type(kString), default_value(false), flag_value(false), int_value(-1), float_value(-1), string_value(
				nullptr), array_value(nullptr), is_set(false) {}
		std::vector <std::string> long_name;
		std::vector <std::string> short_name;
		ArgType arg_type;
		std::string help;
//		std::string action;
		bool default_value;
		bool required;
		bool is_set;
		// This didn't work in a union because of the string value for some reason...
		bool flag_value;
		int int_value;
		float float_value;
		char *string_value;
		char **array_value;

		void set() { is_set = true; }
	};

	CLParser() {
		current_arg_number_ = 0;
		current_arg_name_ = "";

		// Set up the map for readability
		arg_type_map_[0] = "none";
        arg_type_map_[1] = "int";
        arg_type_map_[2] = "string";
        arg_type_map_[3] = "bool";
        arg_type_map_[4] = "float";
        arg_type_map_[5] = "array";

		// Add Help Argument by default
		addArgument("help");
		addArgumentLongName("help");
		addArgumentShortName("h");
		addArgumentType(CLParser::kBool);
		addArgumentHelp("This is the built in help.  Running with this flag will print out help for all possible arguments registered.");
	}

	/**
	 * @brief Add a full argument
	 *
	 * This will add an entire argument when given the following
	 * @param long_name This is the argument long name (without the '--')
	 * @param short_name This is the argument short name (without the '-')
	 * @param arg_type
	 * @param default_val
	 * @param help
	 */
//	void addFullArgument(std::string &long_name, std::string &short_name, ArgType arg_type, void * default_val, std::string &help);
	/**
	 * @brief Add a full argument (sans short name)
	 *
	 * This adds a full argument without the short name.
	 * @param long_name This is the argument long name (without the '--')
	 * @param arg_type
	 * @param default_val
	 * @param help
	 */
//	void addFullArgument(std::string &long_name, ArgType arg_type, void * default_val, std::string &help);

	/**
	 * @brief Add a full argument (with vectors of long and short names)
	 *
	 * @param long_name
	 * @param short_name
	 * @param arg_type
	 * @param default_val
	 * @param help
	 */
//	void addFullArgument(std::vector<std::string> &long_name, std::vector<std::string> &short_name, ArgType arg_type, void * default_val, std::string &help);

	/**
	 * @brief Register argument.
	 * Register an argument with a generic argument name.
	 * @return false on failure, true on success.
	 */
	bool addArgument() {
		std::stringstream arg_name;
		arg_name << "Arg" << current_arg_number_;
		if (registered_arguments_.find(arg_name.str()) != registered_arguments_.end()) {
			std::cerr << "The argument " << arg_name.str() << " is currently in use.  NOT registering argument.";
			return false;
		}
		Argument new_arg = Argument();
		registered_arguments_[arg_name.str()] = new_arg;
		current_arg_number_++;
		current_arg_name_ = arg_name.str();
		return true;
	}

	/**
	 * @brief Registers argument.
	 * Registers a new argument and names it for reference.
	 * @param argument_name General name for the argument for future referencing
	 * @return false on failure, true on success.
	 */
	bool addArgument(const std::string& argument_name) {
		if (registered_arguments_.find(argument_name) != registered_arguments_.end()) {
			std::cerr << "The argument " << argument_name << " is currently in use.  NOT registering argument.";
			return false;
		}
		Argument new_arg = Argument();
		registered_arguments_[argument_name] = new_arg;
		current_arg_name_ = argument_name;
		return true;
	}

	/**
	 * @brief Add a long name
	 *
	 * Add an argument long name (without the '--' included).
	 * @param long_name
	 */
	bool addArgumentLongName(const std::string &long_name) {
		if (std::find(registered_arguments_[current_arg_name_].long_name.begin(),
					  registered_arguments_[current_arg_name_].long_name.end(), long_name)
					  != registered_arguments_[current_arg_name_].long_name.end()) {
			// Already present.
			std::cerr << "Long name --" << long_name << " is already present in this argument (" << current_arg_name_ << ") long name parameters.  Skipping.\n";
			return false;
		}
		registered_arguments_[current_arg_name_].long_name.push_back(long_name);
		return true;
	}

 	/**
	 *
	 * @param long_name
	 */
	bool addArgumentLongName(std::vector<std::string> &long_name) {
		bool success = true;
		for (auto &it : long_name) {
			if (std::find(registered_arguments_[current_arg_name_].long_name.begin(),
						  registered_arguments_[current_arg_name_].long_name.end(), it)
				!= registered_arguments_[current_arg_name_].long_name.end()) {
				// Already present.
				std::cerr << "Long name --" << it << " is already present in this argument (" << current_arg_name_ << ") long name parameters.  Skipping.\n";
				success = false;
			}
			registered_arguments_[current_arg_name_].long_name.push_back(it);
		}
		return success;
	}
	/**
	 *
	 * @param short_name
	 */
	bool addArgumentShortName(const std::string &short_name) {
		if (std::find(registered_arguments_[current_arg_name_].long_name.begin(),
					  registered_arguments_[current_arg_name_].long_name.end(), short_name)
			!= registered_arguments_[current_arg_name_].long_name.end()) {
			// Already present.
			std::cerr << "Short name --" << short_name << " is already present in this argument (" << current_arg_name_ << ") short name parameters.  Skipping.\n";
			return false;
		}
		registered_arguments_[current_arg_name_].short_name.push_back(short_name);
		return true;
	}

	/**
	 *
	 * @param short_name
	 */
	bool addArgumentShortName(std::vector<std::string> &short_name) {
		bool success = true;
		for (auto &it : short_name) {
			if (std::find(registered_arguments_[current_arg_name_].long_name.begin(),
						  registered_arguments_[current_arg_name_].long_name.end(), it)
				!= registered_arguments_[current_arg_name_].long_name.end()) {
				// Already present.
				std::cerr << "Short name --" << it << " is already present in this argument (" << current_arg_name_ << ") short name parameters.  Skipping.\n";
				success = false;
			}
			registered_arguments_[current_arg_name_].short_name.push_back(it);
		}
		return success;
	}
	/**
	 *
	 * @param argument_type
	 */
	void addArgumentType(ArgType argument_type) {
		registered_arguments_[current_arg_name_].arg_type = argument_type;
		if (argument_type == kBool) {
			// Default flag value is false (or OFF) if not pre-set
			if (!registered_arguments_[current_arg_name_].default_value) {
				registered_arguments_[current_arg_name_].flag_value = false;
				registered_arguments_[current_arg_name_].default_value = true;
			}
		} else if (argument_type == kFloat) {
			// Set default value if not already set for FLOAT as 0.0
			if (!registered_arguments_[current_arg_name_].default_value) {
				registered_arguments_[current_arg_name_].float_value = 0.0;
				registered_arguments_[current_arg_name_].default_value = true;
			}
		} else if (argument_type == kInt) {
			// Set default value if not already set for INT as 0
			if (!registered_arguments_[current_arg_name_].default_value) {
				registered_arguments_[current_arg_name_].int_value = 0;
				registered_arguments_[current_arg_name_].default_value = true;
			}
		} else {
			// TODO: Would be an error state here I guess.  I.e. a NoneType
		}
	}

	/**
	 * @brief Set Default Value
	 *
	 * Set default value (int override).
	 * @param default_value
	 */
	void addArgumentDefaultValue(int default_value) {
		registered_arguments_[current_arg_name_].int_value = default_value;
		registered_arguments_[current_arg_name_].default_value = true;
	}
	/**
	 * @brief Set Default Value
	 *
	 * Set default value (float override).
	 * @param default_value
	 */
	void addArgumentDefaultValue(float default_value) {
		registered_arguments_[current_arg_name_].float_value = default_value;
		registered_arguments_[current_arg_name_].default_value = true;
	}

	/**
	 * @brief Set Default Value
	 *
	 * Set default value (bool override).
	 * @param default_value
	 */
	void addArgumentDefaultValue(bool default_value) {
		registered_arguments_[current_arg_name_].flag_value = default_value;
		registered_arguments_[current_arg_name_].default_value = true;
	}

	/**
	 * @brief Set Default Value
	 *
	 * Set default value (string override).
	 * @param default_value
	 */

	void addArgumentDefaultValue(const std::string &default_value) {
		registered_arguments_[current_arg_name_].string_value = strdup(default_value.c_str());
		registered_arguments_[current_arg_name_].default_value = true;
	}

    /**
     * @brief Set Default Value
     *
     * Set default value (const char * override).
     * @param default_value
     */

    void addArgumentDefaultValue(const char *default_value) {
        registered_arguments_[current_arg_name_].string_value = strdup(default_value);
        registered_arguments_[current_arg_name_].default_value = true;
    }

	/**
	 *
	 * @param help
	 */
	void addArgumentHelp(const std::string &help) {
		registered_arguments_[current_arg_name_].help = help;
	}

	void addArgumentRequired(bool required) {
		registered_arguments_[current_arg_name_].required = required;
	}

	/**
	 *
	 * @param set_arg_name
	 * @return True if successfully set, false if failed.
	 */
	bool setCurrentArgument(const std::string &set_arg_name) {
		if (registered_arguments_.count(set_arg_name) > 0) {
			// Found the argument, can set to current.
			current_arg_name_ = set_arg_name;
			return true;
		}
		return false;
	}

	/**
	 * @brief Retrieve argument's set value
	 *
	 * @param argument_name Argument name to search for
	 * @param argument_value Reference to a variable to populate with value.
	 * @return Enumeration with success or error code.
	 */
	FetchValue getArgumentValueByName(const std::string &argument_name, int &argument_value) {
        if (registered_arguments_.count(argument_name)) {
            if (registered_arguments_[argument_name].arg_type == kInt) {
                // Correct Value type
                if (registered_arguments_[argument_name].is_set || registered_arguments_[argument_name].default_value) {
                    argument_value = registered_arguments_[argument_name].int_value;
                } else {
                    return kFetchValueNoValueEntered;
                }
            } else {
                std::cerr << "When fetching the value for argument " << argument_name << " an 'int' was provided but the type is not set as 'int'.\n";
                std::cerr << "The argument " << argument_name << " is set as " << arg_type_map_[registered_arguments_[argument_name].arg_type] << " internally.\n";
                return kFetchValueErrorWrongType;
            }
        } else {
            // Key doesn't exist
            return kFetchValueErrorNoArgRegistered;
        }
        return kFetchValueSuccess;
	}

    /**
     * @brief Retrieve argument's set value
     *
     * @param argument_name Argument name to search for
     * @param argument_value Reference to a variable to populate with value.
     * @return Enumeration with success or error code.
     */
     // TODO: This function can just be a template function.
     FetchValue getArgumentValueByName(const std::string &argument_name, std::string &argument_value) {
        if (registered_arguments_.count(argument_name)) {
            if (registered_arguments_[argument_name].arg_type == kString) {
                // Correct Value type
                if (registered_arguments_[argument_name].is_set || registered_arguments_[argument_name].default_value) {
                    argument_value = registered_arguments_[argument_name].string_value;
                } else {
                    return kFetchValueNoValueEntered;
                }
            } else {
                std::cerr << "When fetching the value for argument " << argument_name << " an 'string' was provided but the type is not set as 'string'.\n";
                std::cerr << "The argument " << argument_name << " is set as " << arg_type_map_[registered_arguments_[argument_name].arg_type] << " internally.\n";
                return kFetchValueErrorWrongType;
            }
        } else {
            // Key doesn't exist
            return kFetchValueErrorNoArgRegistered;
        }
        return kFetchValueSuccess;
    }
    /**
     * @brief Retrieve argument's set value
     *
     * @param argument_name Argument name to search for
     * @param argument_value Reference to a variable to populate with value.
     * @return Enumeration with success or error code.
     */
    FetchValue getArgumentValueByName(const std::string &argument_name, float &argument_value) {
        if (registered_arguments_.count(argument_name)) {
            if (registered_arguments_[argument_name].arg_type == kFloat) {
                // Correct Value type
                if (registered_arguments_[argument_name].is_set || registered_arguments_[argument_name].default_value) {
                    argument_value = registered_arguments_[argument_name].float_value;
                } else {
                    return kFetchValueNoValueEntered;
                }
            } else {
                std::cerr << "When fetching the value for argument " << argument_name << " an 'float' was provided but the type is not set as 'float'.\n";
                std::cerr << "The argument " << argument_name << " is set as " << arg_type_map_[registered_arguments_[argument_name].arg_type] << " internally.\n";
                return kFetchValueErrorWrongType;
            }
        } else {
            // Key doesn't exist
            return kFetchValueErrorNoArgRegistered;
        }
        return kFetchValueSuccess;
    }

    /**
     * @brief Retrieve argument's set value
     *
     * @param argument_name Argument name to search for
     * @param argument_value Reference to a variable to populate with value.
     * @return Enumeration with success or error code.
     */
	bool getArgumentValueByName(const std::string &argument_name, bool &argument_value) {
        if (registered_arguments_.count(argument_name)) {
            if (registered_arguments_[argument_name].arg_type == kBool) {
                // Correct Value type
                if (registered_arguments_[argument_name].is_set || registered_arguments_[argument_name].default_value) {
                    argument_value = registered_arguments_[argument_name].flag_value;
                } else {
                    return kFetchValueNoValueEntered;
                }
            } else {
                std::cerr << "When fetching the value for argument " << argument_name << " an 'bool' was provided but the type is not set as 'bool'.\n";
                std::cerr << "The argument " << argument_name << " is set as " << arg_type_map_[registered_arguments_[argument_name].arg_type] << " internally.\n";
                return kFetchValueErrorWrongType;
            }
        } else {
            // Key doesn't exist
            return kFetchValueErrorNoArgRegistered;
        }
        return kFetchValueSuccess;
    }

	void printArgumentHelp() {
		std::cout << "\n\n";
		std::cout << "Command Line Parameter Help\n";
		std::cout << "===========================\n";
		std::cout << "Usage:\n";
		std::cout << "\t" << program_name_ << "[OPTIONS]\n\n";
		std::cout << "Options:\n";
		std::cout << "-------\n";

		for (auto const& [key, val] : registered_arguments_)
		{
		    std::cout << key << "\n";
				// Get and writeout long names
		    std::stringstream long_names;
		    for (auto const & ln : val.long_name) {
		        long_names << "--" << ln << ", ";
		    }
		    std::cout << "  " << long_names.str();
				// Get and write out short names
				std::stringstream short_names;
				for (auto const & sn : val.short_name) {
						short_names << "-" << sn << ", ";
				}
				std::cout << "\t" << short_names.str();

				// Write out help
				std::cout << "\t" << val.help << "\n";
		}
	}


	/**
	 * @brief Parse given arguments given argument template designed.
	 *
	 * @param argc Count of the arguments from the command line.
	 * @param argv Vector of arguments from command line.
	 *
	 * @return False if failed; true if finished.  Will exit if -h or --help is given.
	 */
	bool parseArguments(int argc, char** argv);

	std::map <const std::string, Argument> registered_arguments_;
	int current_arg_number_;
	std::string current_arg_name_;
	std::string program_name_;
    std::map<int, std::string> arg_type_map_;


};


#endif //CLPARSER_CLPARSER_H
