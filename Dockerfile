# Base Build
FROM registry.gitlab.com/syntheticgio/clparser AS builder

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -qq && apt install -y make cmake build-essential python3-dev

WORKDIR /CLParser
COPY src/ src/
COPY include/ include/
COPY main.cpp .
COPY LICENSE ./
COPY CMakeLists.txt .

#WORKDIR build/
CMD ["/bin/bash"]

# Debug Build
FROM builder AS debug
RUN cmake -DCMAKE_BUILD_TYPE=Debug -S . -B build
RUN cmake --build build

CMD ["/bin/bash"]

# Release Build
FROM builder AS release
RUN cmake -DCMAKE_BUILD_TYPE=Release -S . -B build
RUN cmake --build build

CMD ["/bin/bash"]
